<?php

namespace AppBundle\Controller;

use AppBundle\Utils\ReservaUtils;
use AppBundle\Entity\Comunidad;
use AppBundle\Entity\Equipo;
use AppBundle\Entity\Privilegio;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Comunidad controller.
 *
 * @Route("comunidad")
 */
class ComunidadController extends Controller {

    /**
     * Recibe la fecha seleccionada para la reserva y verifica los bloques libres, en caso de existir reservas (Ajax)
     *
     * @Route("/procesarajax", name="procesar_reserva_ajax")
     * @Method({"GET", "POST"})
     */
    public function procesarReservaAjaxAction(Request $request) {

        $bloques_libres = null;
        $reservas = null;

        $idEquipamiento = $request->get('equipamiento');
        $idBloque = $request->get('bloque');
        $fecha = date_create_from_format('d-m-Y', $request->get('fecha'));
        $fecha = new \DateTime(date_format($fecha, 'Y-m-d'));

        $em = $this->getDoctrine()->getManager();

        # Consultando el bloque y el equipamiento
        $bloque = $em->getRepository('AppBundle:Bloque')->find($idBloque);
        $equipamiento = $em->getRepository('AppBundle:Equipamiento')->find($idEquipamiento);

        $bloqueCompleto = new ReservaUtils(date_format($bloque->getHorainicio(), 'H:i'), date_format($bloque->getHorafin(), 'H:i'));
        
        $misreservas = $em->getRepository('AppBundle:Reserva')->findBy(array('fecha' => $fecha, 'equipamiento' => $equipamiento, 'bloque' => $bloque));
        foreach ($misreservas as $r) {            
            $reserva = new ReservaUtils(date_format($r->getHoradesde(), 'H:i'), date_format($r->getHorahasta(), 'H:i'));
            $reservas[] = $reserva;            
        }

        $bloques_libres = $this->forward('AppBundle:Reserva:obtenerbloquesLibres', array(
            'bloque' => $bloqueCompleto,
            'reservas' => $reservas,
        ));

        $bloques_libres = json_decode($bloques_libres->getContent());

        $response = new JsonResponse();
        $response->setData($bloques_libres);

        return $response;
    }

    /**
     * Dado un dia, retorna los dias de la semana deshabilitados para el Calendario Datepicker
     */
    public function getDiasDeshabilitados($dia) {

        switch ($dia) {
            case "LU":
                $deshabilitar = "0,2,3,4,5,6";
                break;
            case "MA":
                $deshabilitar = "0,1,3,4,5,6";
                break;
            case "MI":
                $deshabilitar = "0,1,2,4,5,6";
                break;
            case "JU":
                $deshabilitar = "0,1,2,3,5,6";
                break;
            case "VI":
                $deshabilitar = "0,1,2,3,4,6";
                break;
            case "SA":
                $deshabilitar = "0,1,2,3,4,5";
                break;
            case "DO":
                $deshabilitar = "1,2,3,4,5,6";
                break;
        }

        return $deshabilitar;
    }

    /**
     * Dado un dia, retorna el dia de la semana habilitado para resaltar en el Calendario Datepicker
     */
    public function getDiaHabilitado($dia) {

        switch ($dia) {
            case "LU":
                $habilitar = "1";
                break;
            case "MA":
                $habilitar = "2";
                break;
            case "MI":
                $habilitar = "3";
                break;
            case "JU":
                $habilitar = "4";
                break;
            case "VI":
                $habilitar = "5";
                break;
            case "SA":
                $habilitar = "6";
                break;
            case "DO":
                $habilitar = "0";
                break;
        }

        return $habilitar;
    }

    /**
     * Lista bloques de horario asociada a la comunidad y el equipamiento
     *
     * @Route("/{id}/equipamientos/reserva", name="bloques_reserva")
     * @Method({"GET", "POST"})
     */
    public function bloquesReservaAction(Request $request) {

        $bloques = null;

        $usuario = $this->getUser();

        $idComunidad = $request->get('comunidad');
        $idEquipamiento = $request->get('equipamiento');
        $idBloque = $request->get('bloque');

        $em = $this->getDoctrine()->getManager();

        # Consultando la comunidad, el equipamiento y el bloque especificado
        $comunidad = $em->getRepository('AppBundle:Comunidad')->find($idComunidad);
        $equipamiento = $em->getRepository('AppBundle:Equipamiento')->find($idEquipamiento);
        $bloque = $em->getRepository('AppBundle:Bloque')->find($idBloque);

        # Consultando que la comunidad tenga el privilegio para el usuario en sesion
        $idsComunidades = $em->getRepository('AppBundle:Comunidad')->getIdComunidadesUsuario($usuario);
  
        # Validando que la $comunidad a consultar este en $idsComunidades
        if (in_array($comunidad->getId(), $idsComunidades)) {

            # Consulto el $equipamiento y sus bloques
            $equipamiento = $em->getRepository('AppBundle:Equipamiento')->find($idEquipamiento);
            $bloques = $em->getRepository('AppBundle:Bloque')->findBy(array('equipamiento' => $equipamiento));

            $diasdeshabilitados = $this->getDiasDeshabilitados($bloque->getDia());
            $diahabilitado = $this->getDiaHabilitado($bloque->getDia());

            return $this->render('comunidad/reserva.html.twig', array(
                        'comunidad' => $comunidad,
                        'equipamiento' => $equipamiento,
                        'bloque' => $bloque,
                        'diasdeshabilitados' => $diasdeshabilitados,
                        'diahabilitado' => $diahabilitado
            ));
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Lista los equipamientos asociados a la $comunidad
     *
     * @Route("/{id}/equipamientos", name="lista_equipamientos")
     * @Method("GET")
     */
    public function mostrarEquipamientosAction(Comunidad $comunidad) {

        $bloques = null;
        $mensaje = null;

        $usuario = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $idsComunidades = $em->getRepository('AppBundle:Comunidad')->getIdComunidadesUsuario($usuario);

        # Validando que la $comunidad a consultar este en $idsComunidades
        if (in_array($comunidad->getId(), $idsComunidades)) {

            # Consultando los equipamientos de la $comunidad
            $equipamientos = $em->getRepository('AppBundle:Equipamiento')->findBy(array('comunidad' => $comunidad));

            if (count($equipamientos) > 0) {
                foreach ($equipamientos as $eq) {
                    $bloques[$eq->getId()] = $em->getRepository('AppBundle:Bloque')->findBy(array('equipamiento' => $eq));
                }
            }

            $bitacora = $this->get('BitacoraServices');
            $bitacora->agregarBitacora($usuario, "CONSULTAR_CALENDARIO", "(" . $comunidad->getId() . ") " . $comunidad->getNombre());

            return $this->render('comunidad/equipamientos.html.twig', array(
                        'comunidad' => $comunidad,
                        'equipamientos' => $equipamientos,
                        'bloques' => $bloques,
                        'mensaje_reserva' => $mensaje
            ));
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Cede la administracion de la $comunidad al $usuario especificado
     *
     * @Route("/ceder", name="ceder")
     * @Method({"GET", "POST"})
     */
    public function cederAction(Request $request) {

        $idEquipos = [];

        $usuario = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $idUsuario = $request->get('idu');
        $idComunidad = $request->get('idc');

        $idsComunidades = $em->getRepository('AppBundle:Comunidad')->getIdComunidades($usuario, Comunidad::ROL_ADMIN);

        # Validando que la $comunidad a consultar este en $idsComunidades para poder ceder la administracion
        if (in_array($idComunidad, $idsComunidades)) {
            $usuarioMiembro = $this->getUser();
            $usuarioAdmin = $em->getRepository('AppBundle:Usuario')->find($idUsuario);
            $comunidad = $em->getRepository('AppBundle:Comunidad')->find($idComunidad);
            $rolAdmin = $em->getRepository('AppBundle:Rol')->find(Comunidad::ROL_ADMIN);
            $rolMiembro = $em->getRepository('AppBundle:Rol')->find(Comunidad::ROL_USER);

            # Modificar privilegio de la comunidad para el Admin y Miembro
            $padmin = $em->getRepository('AppBundle:Privilegio')->updatePrivilegio($comunidad, $usuarioAdmin, $rolAdmin);
            $pmiembro = $em->getRepository('AppBundle:Privilegio')->updatePrivilegio($comunidad, $usuarioMiembro, $rolMiembro);

            # Consulto los $equipos que tienen equipamiento para la $comunidad
            $idEquipos = $em->getRepository('AppBundle:Equipo')->getidEquiposCeder($usuarioMiembro, $comunidad);

            if (count($idEquipos) > 0) {
                # Copiar $equipos para el $usuarioAdmin en tabla equipo
                foreach ($idEquipos as $idEq) {
                    $eq = $em->getRepository('AppBundle:Equipo')->find($idEq);

                    $equipo = new Equipo();
                    $equipo->setNombre($eq->getNombre());
                    $equipo->setDescripcion($eq->getDescripcion());
                    $equipo->setUsuario($usuarioAdmin);
                    $em->persist($equipo);
                    $em->flush($equipo);
                }
            }

            $bitacora = $this->get('BitacoraServices');
            $bitacora->agregarBitacora($usuario, "CEDER_ADMINISTRACION", "(" . $idComunidad . ") Admin: " . $usuarioAdmin->getId() . " - Miembro: " . $usuarioMiembro->getId());

            return $this->redirectToRoute('comunidad_index');
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Lista los usuarios miembros de la $comunidad especificada
     *
     * @Route("/{id}/ceder", name="comunidad_ceder")
     * @Method("GET")
     */
    public function mostrarUsuariosCederAction(Comunidad $comunidad) {
        $usuario = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $idsComunidades = $em->getRepository('AppBundle:Comunidad')->getIdComunidades($usuario, Comunidad::ROL_ADMIN);

        # Validando que la $comunidad a consultar este en $idsComunidades para poder ceder la administracion
        if (in_array($comunidad->getId(), $idsComunidades)) {
            # Consultando los usuarios miembros de la $comunidad
            $miembros = $em->getRepository('AppBundle:Privilegio')->getMiembrosComunidad($comunidad, Comunidad::ROL_USER);

            # Cantidad de miembros vinculados a la $comunidad
            $cantidad = $em->getRepository('AppBundle:Privilegio')->getCantidadMiembros($comunidad);

            return $this->render('comunidad/ceder.html.twig', array(
                        'miembros' => $miembros,
                        'idComunidad' => $comunidad->getId(),
                        'cantidad' => $cantidad
            ));
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Lista el nombre, foto y direccion de la comunidad
     *
     * @Route("/{id}/detalle", name="comunidad_detalle")
     * @Method("GET")
     */
    public function detalleComunidadAction(Comunidad $comunidad) {
        $usuario = $this->getUser();

        # Consultando el rol del $usuario para la $comunidad
        $em = $this->getDoctrine()->getManager();
        $rol = $em->getRepository('AppBundle:Privilegio')->getPrivilegioRol($usuario, $comunidad->getId());

        # Consultando si hay solicitudes de vinculacion con estatus 'P' (Pendiente) para la $comunidad
        $vinculo = $em->getRepository('AppBundle:Vinculo')->getVinculoComunidad($comunidad->getId(), $usuario);

        if ($vinculo) {
            $var = true;
        } else {
            $var = false;
        }

        $bitacora = $this->get('BitacoraServices');
        $bitacora->agregarBitacora($usuario, "CONSULTAR_COMUNIDAD_DETALLE", "(" . $comunidad->getId() . ") " . $comunidad->getNombre());

        return $this->render('comunidad/detalle.html.twig', array(
                    'comunidad' => $comunidad,
                    'rol' => $rol,
                    'vinculo' => $var
        ));
    }

    /**
     * Lista nombre y apellido / username de los usuarios miembros de la $comunidad
     *
     * @Route("/{id}/miembros", name="lista_miembros")
     * @Method("GET")
     */
    public function mostrarMiembrosAction(Comunidad $comunidad) {

        $usuario = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $idsComunidades = $em->getRepository('AppBundle:Comunidad')->getIdComunidadesUsuario($usuario);

        # Validando que la $comunidad a consultar este en $idsComunidades
        if (in_array($comunidad->getId(), $idsComunidades)) {
            # Consultando los $usuarios (miembros) vinculados a la $comunidad especificada            
            $miembros = $em->getRepository('AppBundle:Privilegio')->findBy(array('comunidad' => $comunidad));

            $bitacora = $this->get('BitacoraServices');
            $bitacora->agregarBitacora($usuario, "CONSULTAR_MIEMBROS", "(" . $comunidad->getId() . ") " . $comunidad->getNombre());

            return $this->render('comunidad/miembros.html.twig', array(
                        'comunidad' => $comunidad,
                        'miembros' => $miembros,
            ));
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Lista el detalle de los usuarios miembros de $comunidad
     *
     * @Route("/{id}/miembros/detalle", name="detalle_miembros")
     * @Method("GET")
     */
    public function detalleMiembrosAction(Comunidad $comunidad) {

        $usuario = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $idsComunidades = $em->getRepository('AppBundle:Comunidad')->getIdComunidadesUsuario($usuario);

        # Validando que la $comunidad a consultar este en $idsComunidades
        if (in_array($comunidad->getId(), $idsComunidades)) {
            # Consultando los $usuarios (miembros) vinculados a la $comunidad especificada            
            $miembros = $em->getRepository('AppBundle:Privilegio')->findBy(array('comunidad' => $comunidad));

            return $this->render('comunidad/detallemiembros.html.twig', array(
                        'comunidad' => $comunidad,
                        'miembros' => $miembros,
            ));
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Lista las comunidades a las cuales el $usuario tiene privilegios (Admin o Usuario)
     *
     * @Route("/list", name="miscomunidades_index")
     * @Method("GET")
     */
    public function misComunidadesAction() {
        $miembros = array();
        $roles = array();

        $usuario = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $comunidades = $em->getRepository('AppBundle:Comunidad')->getComunidadesPrivilegio($usuario, 0);

        // Arreglo $miembros[] tiene la cantidad de miembros para cada comunidad en $comunidades
        foreach ($comunidades as $com) {
            $miembros[] = $em->getRepository('AppBundle:Privilegio')->getCantidadMiembros($com->getId());
            $roles[] = $em->getRepository('AppBundle:Privilegio')->getPrivilegioRol($usuario, $com->getId());
        }

        return $this->render('comunidad/miscomunidades.html.twig', array(
                    'comunidades' => $comunidades,
                    'miembros' => $miembros,
                    'roles' => $roles,
        ));
    }

    /**
     * Lists all comunidad entities.
     *
     * @Route("/", name="comunidad_index")
     * @Method("GET")
     */
    public function indexAction() {
        $miembros = array();
        $comunidads = array();

        $usuario = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $comunidads = $em->getRepository('AppBundle:Comunidad')->getComunidadesPrivilegio($usuario, Comunidad::ROL_ADMIN);

        // Array miembros tiene la cantidad de miembros para cada comunidad en $comunidads
        foreach ($comunidads as $com) {
            $miembros[] = $em->getRepository('AppBundle:Privilegio')->getCantidadMiembros($com->getId());
        }

        return $this->render('comunidad/index.html.twig', array(
                    'comunidads' => $comunidads,
                    'miembros' => $miembros,
        ));
    }

    /**
     * Creates a new comunidad entity.
     *
     * @Route("/new", name="comunidad_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $comunidad = new Comunidad();
        $form = $this->createForm('AppBundle\Form\ComunidadType', $comunidad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            # Actualizar campo 'modificado' de la entidad Comunidad
            $comunidad->setModificado(new \DateTime('now'));

            $em->persist($comunidad);
            $em->flush($comunidad);

            $usuario = $this->getUser();

            # Agregar usuario como administrador de la comunidad (privilegio)
            $privilegio = new Privilegio();
            $privilegio->setComunidad($comunidad);
            $privilegio->setUsuario($usuario);

            # Colocando al usuario como Administrador de la comunidad
            $rol = $em->getRepository('AppBundle:Rol')->find(Comunidad::ROL_ADMIN);
            $privilegio->setRol($rol);

            $em->persist($privilegio);
            $em->flush($privilegio);

            $bitacora = $this->get('BitacoraServices');
            $bitacora->agregarBitacora($usuario, "AGREGAR_COMUNIDAD", "(" . $comunidad->getId() . ") " . $comunidad->getNombre());

            return $this->redirectToRoute('comunidad_show', array('id' => $comunidad->getId()));
        }

        return $this->render('comunidad/new.html.twig', array(
                    'comunidad' => $comunidad,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a comunidad entity.
     *
     * @Route("/{id}", name="comunidad_show")
     * @Method("GET")
     */
    public function showAction(Comunidad $comunidad) {
        # Validando privilegios del $usuario
        $usuario = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $idsComunidades = $em->getRepository('AppBundle:Comunidad')->getIdComunidades($usuario, Comunidad::ROL_ADMIN);

        # Validando que la $comunidad a consultar este en $idsComunidades
        if (in_array($comunidad->getId(), $idsComunidades)) {
            $deleteForm = $this->createDeleteForm($comunidad);

            return $this->render('comunidad/show.html.twig', array(
                        'comunidad' => $comunidad,
                        'delete_form' => $deleteForm->createView(),
            ));
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Displays a form to edit an existing comunidad entity.
     *
     * @Route("/{id}/edit", name="comunidad_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Comunidad $comunidad) {
        $usuario = $this->getUser();

        # Equipamientos donde el $usuario es Admin de la comunidad
        $em = $this->getDoctrine()->getManager();
        $idsComunidades = $em->getRepository('AppBundle:Comunidad')->getIdComunidades($usuario, Comunidad::ROL_ADMIN);

        # Validando que la $comunidad a editar este en $idsComunidades
        if (in_array($comunidad->getId(), $idsComunidades)) {
            $deleteForm = $this->createDeleteForm($comunidad);
            $editForm = $this->createForm('AppBundle\Form\ComunidadType', $comunidad);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $comunidad->setModificado(new \DateTime('now'));
                $this->getDoctrine()->getManager()->flush();

                $bitacora = $this->get('BitacoraServices');
                $bitacora->agregarBitacora($usuario, "COMUNIDAD_ACTUALIZADA", "(" . $comunidad->getId() . ") " . $comunidad->getNombre());

                return $this->redirectToRoute('comunidad_edit', array('id' => $comunidad->getId()));
            }

            return $this->render('comunidad/edit.html.twig', array(
                        'comunidad' => $comunidad,
                        'edit_form' => $editForm->createView(),
                        'delete_form' => $deleteForm->createView(),
            ));
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Deletes a comunidad entity.
     *
     * @Route("/{id}", name="comunidad_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Comunidad $comunidad) {
        $form = $this->createDeleteForm($comunidad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comunidad);
            $em->flush();
        }

        return $this->redirectToRoute('comunidad_index');
    }

    /**
     * Creates a form to delete a comunidad entity.
     *
     * @param Comunidad $comunidad The comunidad entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Comunidad $comunidad) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('comunidad_delete', array('id' => $comunidad->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
