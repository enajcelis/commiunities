<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comunidad;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {

        $coordenadas = null;
        $detalle = null;
        $roles = array();

        $usuario = $this->getUser();

        # Consultando latitud/longitud de las comunidades existentes
        $em = $this->getDoctrine()->getManager();
        $comunidades = $em->getRepository('AppBundle:Comunidad')->findAll();

        // Construyendo el arreglo de $coordendas a utilizar en el script del mapa
        foreach ($comunidades as $com) {
            if ($com->getDireccion()->getLatitud() != null && $com->getDireccion()->getLongitud() != null) {
                if (!$coordenadas) {
                    $coordenadas = "{lat: " . $com->getDireccion()->getLatitud() . ", lng: " . $com->getDireccion()->getLongitud() . "}";
                    $detalle = $com->getNombre();
                } else {
                    $coordenadas = $coordenadas . ", {lat: " . $com->getDireccion()->getLatitud() . ", lng: " . $com->getDireccion()->getLongitud() . "}";
                    $detalle = $detalle . ", " . $com->getNombre();
                }
            }

            $roles[] = $em->getRepository('AppBundle:Privilegio')->getPrivilegioRol($usuario, $com->getId());
        }

        return $this->render('default/index.html.twig', array(
                    'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
                    'coordenadas' => $coordenadas,
                    'detalle' => $detalle,
                    'comunidades' => $comunidades,
                    'roles' => $roles
        ));
    }

    /**
     * @Route("/index", name="index")
     */
    public function indexCommiunitiesAction(Request $request) {
        return $this->render('commiunities/index.html.twig', array(
                    'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
        ));
    }

    /**
     * @Route("/mapa", name="mapa_commiunities")
     */
    public function mapaCommiunitiesAction(Request $request) {

        $coordenadas = null;
        $detalle = null;

        # Consultando latitud/longitud de las comunidades existentes
        $em = $this->getDoctrine()->getManager();
        $comunidades = $em->getRepository('AppBundle:Comunidad')->findAll();

        $i = 1;

        // Construyendo el arreglo de $coordendas a utilizar en el script del mapa
        foreach ($comunidades as $com) {
            if ($i == 1) {
                $comunidad1 = $com;
            } elseif ($i == 2) {
                $comunidad2 = $com;
            }
            if ($com->getDireccion()->getLatitud() != null && $com->getDireccion()->getLongitud() != null) {
                if (!$coordenadas) {
                    $coordenadas = "{lat: " . $com->getDireccion()->getLatitud() . ", lng: " . $com->getDireccion()->getLongitud() . "}";
                    $detalle = $com->getNombre();
                } else {
                    $coordenadas = $coordenadas . ", {lat: " . $com->getDireccion()->getLatitud() . ", lng: " . $com->getDireccion()->getLongitud() . "}";
                    $detalle = $detalle . ", " . $com->getNombre();
                }
            }
            $i++;
        }

        return $this->render('commiunities/mapa.html.twig', array(
                    'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
                    'coordenadas' => $coordenadas,
                    'detalle' => $detalle,
                    'comunidad1' => $comunidad1,
                    'comunidad2' => $comunidad2,
                    'comunidades' => $comunidades
        ));
    }

    /**
     * @Route("/ver/{id}", name="ver")
     */
    public function verAction(Comunidad $comunidad) {
        return $this->render('commiunities/ver.html.twig', array(
                    'comunidad' => $comunidad,
        ));
    }

}
