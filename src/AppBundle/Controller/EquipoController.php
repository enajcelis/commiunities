<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Equipo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Equipo controller.
 *
 * @Route("equipo")
 */
class EquipoController extends Controller {

    /**
     * Lista todas las entidades equipo del usuario en sesion
     *
     * @Route("/", name="equipo_index")
     * @Method("GET")
     */
    public function indexAction() {
        $usuario = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $equipos = $em->getRepository('AppBundle:Equipo')->findBy(array('usuario' => $usuario));

        return $this->render('equipo/index.html.twig', array(
                    'equipos' => $equipos,
        ));
    }

    /**
     * Creates a new equipo entity.
     *
     * @Route("/new", name="equipo_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $equipo = new Equipo();
        $form = $this->createForm('AppBundle\Form\EquipoType', $equipo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $usuario = $this->getUser();
            $equipo->setUsuario($usuario);

            $em->persist($equipo);
            $em->flush($equipo);

            # Registrando la accion en la bitacora
            $bitacora = $this->get('BitacoraServices');
            $bitacora->agregarBitacora($usuario, "AGREGAR_EQUIPO", "(" . $equipo->getId() . ") " . $equipo->getNombre() . ", " . $equipo->getTipo());

            return $this->redirectToRoute('equipo_show', array('id' => $equipo->getId()));
        }

        return $this->render('equipo/new.html.twig', array(
                    'equipo' => $equipo,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a equipo entity.
     *
     * @Route("/{id}", name="equipo_show")
     * @Method("GET")
     */
    public function showAction(Equipo $equipo) {

        $reservas = null; $acciones = null; $equipamientos = null;

        # Validando privilegios del $usuario
        $usuario = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $res = $em->getRepository('AppBundle:Equipo')->find($equipo->getId());

        if ($res->getUsuario() == $usuario) {

            # Si hay reservas asociadas al equipo, no se listara la opcion de eliminar en la vista
            $equipos = $em->getRepository('AppBundle:Equipo')->findBy(array('usuario' => $usuario));
            foreach ($equipos as $eqp) {
                # Consultando si el equipo esta asociado a alguna comunidad (equipamiento)
                $equipamientos = $em->getRepository('AppBundle:Equipamiento')->findBy(array('equipo' => $eqp));

                if (count($equipamientos) > 0) {
                    # Verificando si los equipamientos anteriores tienen reservas
                    foreach ($equipamientos as $eq) {
                        $reservas = $em->getRepository('AppBundle:Reserva')->findBy(array('equipamiento' => $eq->getId()));

                        if (count($reservas) > 0) {
                            $acciones[$eqp->getId()] = true;
                        } else {
                            $acciones[$eqp->getId()] = false;
                        }
                    }
                }
            }

            $deleteForm = $this->createDeleteForm($equipo);

            return $this->render('equipo/show.html.twig', array(
                        'equipo' => $equipo,
                        'delete_form' => $deleteForm->createView(),
                        'acciones' => $acciones
            ));
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Displays a form to edit an existing equipo entity.
     *
     * @Route("/{id}/edit", name="equipo_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Equipo $equipo) {
        # Validando privilegios del $usuario
        $usuario = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $res = $em->getRepository('AppBundle:Equipo')->find($equipo->getId());

        if ($res->getUsuario() == $usuario) {
            $deleteForm = $this->createDeleteForm($equipo);
            $editForm = $this->createForm('AppBundle\Form\EquipoType', $equipo);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('equipo_edit', array('id' => $equipo->getId()));
            }

            # Registrando la accion en la bitacora
            $bitacora = $this->get('BitacoraServices');
            $bitacora->agregarBitacora($usuario, "EQUIPO_ACTUALIZADO", "(" . $equipo->getId() . ") " . $equipo->getNombre() . ", " . $equipo->getTipo());

            return $this->render('equipo/edit.html.twig', array(
                        'equipo' => $equipo,
                        'edit_form' => $editForm->createView(),
                        'delete_form' => $deleteForm->createView(),
            ));
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Deletes a equipo entity.
     *
     * @Route("/{id}", name="equipo_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Equipo $equipo) {
        $form = $this->createDeleteForm($equipo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usuario = $this->getUser();
            
            # Registrando la accion en la bitacora
            $bitacora = $this->get('BitacoraServices');
            $bitacora->agregarBitacora($usuario, "EQUIPO_ELIMINADO", "(" . $equipo->getId() . ") " . $equipo->getNombre());

            $em = $this->getDoctrine()->getManager();
            $em->remove($equipo);
            $em->flush();
        }

        return $this->redirectToRoute('equipo_index');
    }

    /**
     * Creates a form to delete a equipo entity.
     *
     * @param Equipo $equipo The equipo entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Equipo $equipo) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('equipo_delete', array('id' => $equipo->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
