<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Bloque;
use AppBundle\Entity\Comunidad;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Bloque controller.
 *
 * @Route("bloque")
 */
class BloqueController extends Controller {

    /**
     * Lists all bloque entities.
     *
     * @Route("/", name="bloque_index")
     * @Method("GET")
     */
    public function indexAction() {

        $reservas = null; $acciones = null;

        $request = $this->getRequest();
        $ideq = $request->get('ideq');

        $usuario = $this->getUser();

        # Consultando el $equipamiento 
        $em = $this->getDoctrine()->getManager();
        $equipamiento = $em->getRepository('AppBundle:Equipamiento')->find($ideq);

        # Equipamientos donde el $usuario es Admin de la comunidad
        $idsEquipamientos = $em->getRepository('AppBundle:Equipamiento')->getIdEquipamientos($usuario, Comunidad::ROL_ADMIN);

        # Validando que el $ideq a consultar bloques este en $idsEquipamientos
        if (in_array($ideq, $idsEquipamientos)) {
            $bloques = $em->getRepository('AppBundle:Bloque')->findBy(array('equipamiento' => $ideq));

            # Si hay reservas asociadas para el bloque, no se listara la opcion de editar en la vista
            foreach ($bloques as $b) {
                $reservas = $em->getRepository('AppBundle:Reserva')->findBy(array('bloque' => $b->getId()));

                if (count($reservas) > 0) {
                    $acciones[$b->getId()] = true;
                } else {
                    $acciones[$b->getId()] = false;
                }
            }

            return $this->render('bloque/index.html.twig', array(
                        'bloques' => $bloques,
                        'equipamiento' => $equipamiento,
                        'acciones' => $acciones
            ));
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Creates a new bloque entity.
     *
     * @Route("/new/{ideq}", name="bloque_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $ideq = $request->get('ideq');

        $usuario = $this->getUser();

        # Consultando el $equipamiento 
        $em = $this->getDoctrine()->getManager();
        $equipamiento = $em->getRepository('AppBundle:Equipamiento')->find($ideq);

        # Equipamientos donde el $usuario es Admin de la comunidad
        $idsEquipamientos = $em->getRepository('AppBundle:Equipamiento')->getIdEquipamientos($usuario, Comunidad::ROL_ADMIN);

        # Validando que el $ideq a consultar bloques este en $idsEquipamientos
        if (in_array($ideq, $idsEquipamientos)) {

            $bloque = new Bloque();
            $form = $this->createForm('AppBundle\Form\BloqueType', $bloque, ['ideq' => $ideq]);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($bloque);
                $em->flush();

                # Registrando la accion en la bitacora
                $bitacora = $this->get('BitacoraServices');
                $bitacora->agregarBitacora($usuario, "AGREGAR_BLOQUE", "(" . $bloque->getId() . ") E: " . $bloque->getEquipamiento()->getId() . ", " . $bloque->getDia() . ", desde " . date_format($bloque->getHorainicio(), 'H:i:s') . ", hasta " . date_format($bloque->getHorafin(), 'H:i:s'));

                return $this->redirectToRoute('bloque_show', array('id' => $bloque->getId(), 'ideq' => $ideq));
            }

            return $this->render('bloque/new.html.twig', array(
                        'bloque' => $bloque,
                        'equipamiento' => $equipamiento,
                        'form' => $form->createView(),
            ));
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Finds and displays a bloque entity.
     *
     * @Route("/{id}/{ideq}", name="bloque_show")
     * @Method("GET")
     */
    public function showAction(Bloque $bloque) {

        $reservas = null; $acciones = null;

        $request = $this->getRequest();
        $ideq = $request->get('ideq');

        $usuario = $this->getUser();

        # Consultando el $equipamiento 
        $em = $this->getDoctrine()->getManager();
        $equipamiento = $em->getRepository('AppBundle:Equipamiento')->find($ideq);

        # Bloques asociados a equipamientos donde el $usuario es Admins de la comunidad        
        $idsBloques = $em->getRepository('AppBundle:Bloque')->getIdBloques($usuario, Comunidad::ROL_ADMIN);

        # Validando que el $bloque a consultar este en $idsBloques
        if (in_array($bloque->getId(), $idsBloques)) {

            # Si hay reservas asociadas para el bloque, no se mostrara el botn de editar ni eliminar en la vista
            $bloques = $em->getRepository('AppBundle:Bloque')->findBy(array('equipamiento' => $ideq));
            foreach ($bloques as $b) {
                $reservas = $em->getRepository('AppBundle:Reserva')->findBy(array('bloque' => $b->getId()));

                if (count($reservas) > 0) {
                    $acciones[$b->getId()] = true;
                } else {
                    $acciones[$b->getId()] = false;
                }
            }

            $deleteForm = $this->createDeleteForm($bloque, ['ideq' => $ideq]);

            return $this->render('bloque/show.html.twig', array(
                        'bloque' => $bloque,
                        'equipamiento' => $equipamiento,
                        'delete_form' => $deleteForm->createView(),
                        'acciones' => $acciones
            ));
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Displays a form to edit an existing bloque entity.
     *
     * @Route("/{id}/edit/{ideq}", name="bloque_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Bloque $bloque) {

        $reservas = null;

        $ideq = $request->get('ideq');
        $usuario = $this->getUser();

        # Consultando el $equipamiento 
        $em = $this->getDoctrine()->getManager();
        $equipamiento = $em->getRepository('AppBundle:Equipamiento')->find($ideq);

        # Bloques asociados a equipamientos donde el $usuario es Admins de la comunidad
        $idsBloques = $em->getRepository('AppBundle:Bloque')->getIdBloques($usuario, Comunidad::ROL_ADMIN);

        # Verificando si el $bloque tiene reservas asociadas
        $reservas = $em->getRepository('AppBundle:Reserva')->findBy(array('bloque' => $bloque->getId()));

        # Validando que el $bloque a consultar este en $idsBloques
        if ((in_array($bloque->getId(), $idsBloques)) && ( count($reservas) == 0)) {

            $deleteForm = $this->createDeleteForm($bloque, ['ideq' => $ideq]);
            $editForm = $this->createForm('AppBundle\Form\BloqueType', $bloque, ['ideq' => $ideq]);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                # Registrando la accion en la bitacora
                $bitacora = $this->get('BitacoraServices');
                $bitacora->agregarBitacora($usuario, "BLOQUE_ACTUALIZADO", "(" . $bloque->getId() . ") E: " . $bloque->getEquipamiento()->getId() . ", " . $bloque->getDia() . ", desde " . date_format($bloque->getHorainicio(), 'H:i:s') . ", hasta " . date_format($bloque->getHorafin(), 'H:i:s'));

                return $this->redirectToRoute('bloque_edit', array('id' => $bloque->getId(), 'ideq' => $ideq));
            }

            return $this->render('bloque/edit.html.twig', array(
                        'bloque' => $bloque,
                        'equipamiento' => $equipamiento,
                        'edit_form' => $editForm->createView(),
                        'delete_form' => $deleteForm->createView(),
            ));
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Deletes a bloque entity.
     *
     * @Route("/{id}/{ideq}", name="bloque_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Bloque $bloque) {
        $ideq = $request->get('ideq');

        $form = $this->createDeleteForm($bloque, array('ideq' => $ideq));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usuario = $this->getUser();
            
            # Registrando la accion en la bitacora
            $bitacora = $this->get('BitacoraServices');
            $bitacora->agregarBitacora($usuario, "BLOQUE_ELIMINADO", "(" . $bloque->getId() . ") E: " . $bloque->getEquipamiento()->getId() . ", " . $bloque->getDia() . ", desde " . date_format($bloque->getHorainicio(), 'H:i:s') . ", hasta " . date_format($bloque->getHorafin(), 'H:i:s'));

            $em = $this->getDoctrine()->getManager();
            $em->remove($bloque);
            $em->flush();
        }

        return $this->redirectToRoute('bloque_index', array('ideq' => $ideq));
    }

    /**
     * Creates a form to delete a bloque entity.
     *
     * @param Bloque $bloque The bloque entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Bloque $bloque) {
        $request = $this->getRequest();
        $ideq = $request->get('ideq');

        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('bloque_delete', array('id' => $bloque->getId(), 'ideq' => $ideq)))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
