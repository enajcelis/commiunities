<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Equipamiento;
use AppBundle\Entity\Comunidad;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Equipamiento controller.
 *
 * @Route("equipamiento")
 */
class EquipamientoController extends Controller {

    /**
     * Lists all equipamiento entities.
     *
     * @Route("/", name="equipamiento_index")
     * @Method("GET")
     */
    public function indexAction() {
        
        $reservas = null; $acciones = null;
        
        $usuario = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $cantidad = $em->getRepository('AppBundle:Privilegio')->cantidadComunidades($usuario, Comunidad::ROL_ADMIN);

        # Validando que el usuario tenga comunidades creadas
        if ($cantidad > 0) {
            $equipamientos = $em->getRepository('AppBundle:Equipamiento')->getEquipamientos($usuario);

            # Si hay reservas asociadas al equipamiento, no se listara la opcion de editar en la vista
            foreach ($equipamientos as $eq) {
                $reservas = $em->getRepository('AppBundle:Reserva')->findBy(array('equipamiento' => $eq->getId()));

                if (count($reservas) > 0) {
                    $acciones[$eq->getId()] = true;
                } else {
                    $acciones[$eq->getId()] = false;
                }
            }

            return $this->render('equipamiento/index.html.twig', array(
                        'equipamientos' => $equipamientos,
                        'acciones' => $acciones
            ));
        } else {
            return $this->render('plantilla/error/crearcomunidad.html.twig');
        }
    }

    /**
     * Creates a new equipamiento entity.
     *
     * @Route("/new", name="equipamiento_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $usuario = $this->getUser();

        # Comunidades donde el $usuario es Admin
        $em = $this->getDoctrine()->getManager();
        $comunidades = $em->getRepository('AppBundle:Comunidad')->getComunidadesPrivilegio($usuario, Comunidad::ROL_ADMIN);

        $equipamiento = new Equipamiento();
        $form = $this->createForm('AppBundle\Form\EquipamientoType', $equipamiento, ['usuario' => $usuario, 'comunidades' => $comunidades]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($equipamiento);
            $em->flush($equipamiento);

            # Registrando la accion en la bitacora
            $bitacora = $this->get('BitacoraServices');
            $bitacora->agregarBitacora($usuario, "AGREGAR_EQUIPAMIENTO", "(" . $equipamiento->getId() . ") E: " . $equipamiento->getEquipo()->getId() . " - C: " . $equipamiento->getComunidad()->getId());

            return $this->redirectToRoute('equipamiento_show', array('id' => $equipamiento->getId()));
        }

        return $this->render('equipamiento/new.html.twig', array(
                    'equipamiento' => $equipamiento,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a equipamiento entity.
     *
     * @Route("/{id}", name="equipamiento_show")
     * @Method("GET")
     */
    public function showAction(Equipamiento $equipamiento) {
        
        $reservas = null; $acciones = null;
        
        $usuario = $this->getUser();

        # Equipamientos donde el $usuario es Admin de la comunidad
        $em = $this->getDoctrine()->getManager();
        $idsEquipamientos = $em->getRepository('AppBundle:Equipamiento')->getIdEquipamientos($usuario, Comunidad::ROL_ADMIN);

        # Validando que el $equipamiento a consultar este en $idsEquipamientos
        if (in_array($equipamiento->getId(), $idsEquipamientos)) {
            
            # Si hay reservas asociadas al equipamiento, no se listara la opcion de editar/eliminar en la vista
            $equipamientos = $em->getRepository('AppBundle:Equipamiento')->getEquipamientos($usuario);
            foreach ($equipamientos as $eq) {
                $reservas = $em->getRepository('AppBundle:Reserva')->findBy(array('equipamiento' => $eq->getId()));

                if (count($reservas) > 0) {
                    $acciones[$eq->getId()] = true;
                } else {
                    $acciones[$eq->getId()] = false;
                }
            }
            
            $deleteForm = $this->createDeleteForm($equipamiento);

            return $this->render('equipamiento/show.html.twig', array(
                        'equipamiento' => $equipamiento,
                        'delete_form' => $deleteForm->createView(),
                        'acciones' => $acciones
            ));
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Displays a form to edit an existing equipamiento entity.
     *
     * @Route("/{id}/edit", name="equipamiento_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Equipamiento $equipamiento) {
        
        $reservas = null;
        
        $usuario = $this->getUser();

        # Equipamientos donde el $usuario es Admin de la comunidad
        $em = $this->getDoctrine()->getManager();
        $idsEquipamientos = $em->getRepository('AppBundle:Equipamiento')->getIdEquipamientos($usuario, Comunidad::ROL_ADMIN);
        
        # Verificando si el $equipamiento tiene reservas asociadas
        $reservas = $em->getRepository('AppBundle:Reserva')->findBy(array('equipamiento' => $equipamiento->getId()));

        # Validando que el $equipamiento a editar este en $idsEquipamientos
        if ( (in_array($equipamiento->getId(), $idsEquipamientos)) && ( count($reservas) == 0) ) {
            # Comunidades donde el $usuario es Admin
            $em = $this->getDoctrine()->getManager();
            $comunidades = $em->getRepository('AppBundle:Comunidad')->getComunidadesPrivilegio($usuario, Comunidad::ROL_ADMIN);

            $deleteForm = $this->createDeleteForm($equipamiento);
            $editForm = $this->createForm('AppBundle\Form\EquipamientoType', $equipamiento, ['usuario' => $usuario, 'comunidades' => $comunidades]);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                # Registrando la accion en la bitacora
                $bitacora = $this->get('BitacoraServices');
                $bitacora->agregarBitacora($usuario, "EQUIPAMIENTO_ACTUALIZADO", "(" . $equipamiento->getId() . ") E: " . $equipamiento->getEquipo()->getId() . " - C: " . $equipamiento->getComunidad()->getId());

                return $this->redirectToRoute('equipamiento_edit', array('id' => $equipamiento->getId()));
            }

            return $this->render('equipamiento/edit.html.twig', array(
                        'equipamiento' => $equipamiento,
                        'edit_form' => $editForm->createView(),
                        'delete_form' => $deleteForm->createView(),
            ));
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Deletes a equipamiento entity.
     *
     * @Route("/{id}", name="equipamiento_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Equipamiento $equipamiento) {
        $form = $this->createDeleteForm($equipamiento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usuario = $this->getUser();
            
            # Registrando la accion en la bitacora
            $bitacora = $this->get('BitacoraServices');
            $bitacora->agregarBitacora($usuario, "EQUIPAMIENTO_ELIMINADO", "(" . $equipamiento->getId() . ") E: " . $equipamiento->getEquipo()->getId() . " - C: " . $equipamiento->getComunidad()->getId());

            $em = $this->getDoctrine()->getManager();
            $em->remove($equipamiento);
            $em->flush();
        }

        return $this->redirectToRoute('equipamiento_index');
    }

    /**
     * Creates a form to delete a equipamiento entity.
     *
     * @param Equipamiento $equipamiento The equipamiento entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Equipamiento $equipamiento) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('equipamiento_delete', array('id' => $equipamiento->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
