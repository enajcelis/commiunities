<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Reserva;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Reserva controller.
 *
 * @Route("reserva")
 */
class ReservaController extends Controller {

    /**
     * Arreglo con los bloques horarios libres dado un $bloque y $reservas
     *
     * @Route("/reserva/libres", name="bloques_libres")
     * @Method({"GET", "POST"})
     */
    public function obtenerbloquesLibresAction($bloque, $reservas) {
        $alltimes = array_fill_keys(range($bloque->starttime(), $bloque->endtime()), 1);

        foreach ($reservas as $appt) {
            $alltimes = array_diff_key($alltimes, array_fill_keys(range($appt->starttime() + 1, $appt->endtime() - 1), 1));
        }

        $groups = array();
        $active_group = 0;

        $output = array();
        $output_counter = 0;
        $nums = array_keys($alltimes);

        foreach ($nums as $k => $num) {
            if ($k !== 0 && $nums[$k] !== $nums[$k - 1] + 1) {
                $active_group ++;
            }
            $groups[$active_group][] = $num;
        }

        foreach ($groups as $group) {
            $tmparrayGroup = array_values($group);
            $first = array_shift($tmparrayGroup);
            $output[$output_counter][] = $first;
            $last = array_pop($tmparrayGroup);

            if ($last == null) {
                $last = $first;
            }

            if ($first !== $last) {
                $output[$output_counter][] = $last;
            }
            $output_counter++;
        }

        $i = 0;
        $eliminar = null;
        foreach ($output as &$span) {
            $span[0] = $this->convert_to_time($span[0]);
            if (isset($span[1])) {
                $span[1] = $this->convert_to_time($span[1]);
            } else {
                $eliminar[] = $i;
                $span[1] = $span[0];
            }
            $i++;
        }

        if ($eliminar != null) {
            foreach ($eliminar as $e) {
                unset($output[$e]);
            }
        }

        $arreglo = array_values($output);

        $response = new JsonResponse();
        $response->setData($arreglo);

        return $response;
    }

    public function convert_to_time($minutes) {
        $hour = (int) ($minutes / 60);
        $minutes = $minutes % 60;
        return str_pad($hour, 2, '0', STR_PAD_LEFT) . ':' . str_pad($minutes, 2, '0', STR_PAD_LEFT);
    }

    /**
     * Creates a new reserva entity.
     *
     * @Route("/new", name="reserva_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {

        $mensaje = null;
        $usuario = $this->getUser();

        $idBloque = $request->get('bloque');
        $idComunidad = $request->get('comunidad');
        $idEquipamiento = $request->get('equipamiento');

        $f = date_create_from_format('d-m-Y', $request->get('fecha'));
        $fecha = new \DateTime(date_format($f, 'Y-m-d'));
        $desde = date_create_from_format('H:i:s', $request->get('desde_hora') . ":" . $request->get('desde_minutos') . ":00");
        $horadesde = new \DateTime(date_format($desde, 'H:i:s'));
        $hasta = date_create_from_format('H:i:s', $request->get('hasta_hora') . ":" . $request->get('hasta_minutos') . ":00");
        $horahasta = new \DateTime(date_format($hasta, 'H:i:s'));

        $em = $this->getDoctrine()->getManager();

        # Consultando la comunidad, el equipamiento y el bloque especificado
        $comunidad = $em->getRepository('AppBundle:Comunidad')->find($idComunidad);
        $equipamiento = $em->getRepository('AppBundle:Equipamiento')->find($idEquipamiento);
        $bloque = $em->getRepository('AppBundle:Bloque')->find($idBloque);

        # Agregar reserva
        $reserva = new Reserva();
        $reserva->setFecha($fecha);
        $reserva->setHoradesde($horadesde);
        $reserva->setHorahasta($horahasta);
        $reserva->setEquipamiento($equipamiento);
        $reserva->setBloque($bloque);
        $reserva->setUsuario($usuario);
        $reserva->setComunidad($comunidad);

        $em->persist($reserva);
        $em->flush($reserva);

        # Registrar accion en bitacora
        $bitacora = $this->get('BitacoraServices');
        $bitacora->agregarBitacora($usuario, "RESERVAR", "(" . $reserva->getId() . ") E: " . $reserva->getEquipamiento()->getEquipo()->getId() . " - B: " . $reserva->getBloque()->getId() . " - C: " . $reserva->getComunidad()->getId() . " - U: " . $usuario->getId() . " - fecha: " . date_format($reserva->getFecha(), 'Y-m-d') . " - desde: " . date_format($reserva->getHoradesde(), 'H:i') . " - hasta: " . date_format($reserva->getHorahasta(), 'H:i'));

        # Consultando los equipamientos de la $comunidad
        $equipamientos = $em->getRepository('AppBundle:Equipamiento')->findBy(array('comunidad' => $comunidad));

        if (count($equipamientos) > 0) {
            foreach ($equipamientos as $eq) {
                $bloques[$eq->getId()] = $em->getRepository('AppBundle:Bloque')->findBy(array('equipamiento' => $eq));
            }
        }

        $mensaje = "reserva";

        return $this->render('comunidad/equipamientos.html.twig', array(
                    'comunidad' => $comunidad,
                    'equipamientos' => $equipamientos,
                    'bloques' => $bloques,
                    'mensaje_reserva' => $mensaje
        ));
    }

    /**
     * Lists all reserva entities.
     *
     * @Route("/", name="reserva_index")
     * @Method("GET")
     */
    public function indexAction() {
        $usuario = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $reservas = $em->getRepository('AppBundle:Reserva')->findby(array('usuario' => $usuario));

        return $this->render('reserva/misreservas.html.twig', array(
                    'reservas' => $reservas,
        ));
    }

    /**
     * Finds and displays a reserva entity.
     *
     * @Route("/{id}", name="reserva_show")
     * @Method("GET")
     */
    public function showAction(Reserva $reserva) {

        $usuario = $this->getUser();
        
        $em = $this->getDoctrine()->getManager();

        # Reservas realizadas por el $usuario
        $idsReservas = $em->getRepository('AppBundle:Reserva')->getIdReservas($usuario);

        # Validando que la reserva a consultar este en $idsReservas
        if (in_array($reserva->getId(), $idsReservas)) {
            # Registrar accion en bitacora
            $bitacora = $this->get('BitacoraServices');
            $bitacora->agregarBitacora($usuario, "CONSULTAR_RESERVA", "(" . $reserva->getId() . ") E: " . $reserva->getEquipamiento()->getEquipo()->getId() . ": " . date_format($reserva->getFecha(), 'Y-m-d') . " - desde: " . date_format($reserva->getHoradesde(), 'H:i') . " - hasta: " . date_format($reserva->getHorahasta(), 'H:i'));

            $deleteForm = $this->createDeleteForm($reserva);

            return $this->render('reserva/show.html.twig', array(
                        'reserva' => $reserva,
                        'delete_form' => $deleteForm->createView(),
            ));
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Deletes a reserva entity.
     *
     * @Route("/{id}", name="reserva_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Reserva $reserva) {
        $form = $this->createDeleteForm($reserva);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            # Registrar accion en bitacora
            $usuario = $this->getUser();
            $bitacora = $this->get('BitacoraServices');
            $bitacora->agregarBitacora($usuario, "RESERVA_ELIMINADA", "(" . $reserva->getId() . ") E: " . $reserva->getEquipamiento()->getEquipo()->getId() . " - B: " . $reserva->getBloque()->getId() . " - C: " . $reserva->getComunidad()->getId() . " - U: " . $usuario->getId() . " - fecha: " . date_format($reserva->getFecha(), 'Y-m-d') . " - desde: " . date_format($reserva->getHoradesde(), 'H:i') . " - hasta: " . date_format($reserva->getHorahasta(), 'H:i'));

            $em = $this->getDoctrine()->getManager();
            $em->remove($reserva);
            $em->flush();
        }

        return $this->redirectToRoute('reserva_index');
    }

    /**
     * Creates a form to delete a reserva entity.
     *
     * @param Reserva $reserva The reserva entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Reserva $reserva) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('reserva_delete', array('id' => $reserva->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
