<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Vinculo;
use AppBundle\Entity\Comunidad;
use AppBundle\Entity\Privilegio;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Vinculo controller.
 *
 * @Route("vinculo")
 */
class VinculoController extends Controller {

    /**
     * Gestiona la solicitud de $vinculo (aprobar, negar)
     *
     * @Route("/gestion", name="vinculo_gestion")
     * @Method({"GET", "POST"})
     */
    public function gestionarSolicitudAction(Request $request) {

        $usuario = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $idVinculo = $request->get('id');
        $accion = $request->get('a'); // a: aprobar, n: negar
        # Vinculos de comunidades donde el $usuario es Administrador
        $comunidades = $em->getRepository('AppBundle:Comunidad')->getComunidades($usuario, Comunidad::ROL_ADMIN);
        $idsVinculos = $em->getRepository('AppBundle:Vinculo')->getIdVinculos($comunidades, $usuario);

        # Validando que la solicitud de $vinculo este en $idsVinculos para poder aprobar/negar
        if (in_array($idVinculo, $idsVinculos)) {

            # Modificar estatus del vinculo
            $vinculo = $em->getRepository('AppBundle:Vinculo')->updateVinculo($idVinculo, $accion);

            # En caso de aprobar vinculo, registrar el privilegio con rol 2 (miembro) al $usuario
            if ($accion == 'a') {
                $vinculo = $em->getRepository('AppBundle:Vinculo')->find($idVinculo);

                $privilegio = new Privilegio();
                $privilegio->setComunidad($vinculo->getComunidad());
                $privilegio->setUsuario($vinculo->getUsuario());

                # Colocando al usuario como Miembro de la comunidad
                $rol = $em->getRepository('AppBundle:Rol')->find(Comunidad::ROL_USER);
                $privilegio->setRol($rol);

                $em->persist($privilegio);
                $em->flush($privilegio);

                $accionBitacora = "VINCULO_APROBADO";
            } elseif ($accion == 'n') {
                $accionBitacora = "VINCULO_NEGADO";
            }

            $bitacora = $this->get('BitacoraServices');
            $bitacora->agregarBitacora($usuario, $accionBitacora, "(" . $vinculo->getId() . ") U: " . $vinculo->getUsuario()->getId() . " C: " . $vinculo->getComunidad()->getId());

            return $this->redirectToRoute('recibidas_index');
        } else {
            return $this->render('plantilla/error/forbidden.html.twig');
        }
    }

    /**
     * Lista las solicitudes de vinculacion a comunidades realizadas por el usuario en sesion
     *
     * @Route("/my", name="missolicitudes_index")
     * @Method("GET")
     */
    public function misSolicitudesAction() {
        $usuario = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $solicitudes = $em->getRepository('AppBundle:Vinculo')->getMisSolicitudes($usuario);

        return $this->render('vinculo/missolicitudes.html.twig', array(
                    'solicitudes' => $solicitudes,
        ));
    }

    /**
     * Lista las solicitudes de vinculacion realizadas por usuarios a las comunidades administradas por el usuario en sesion
     *
     * @Route("/received", name="recibidas_index")
     * @Method("GET")
     */
    public function solicitudesRecibidasAction() {
        $usuario = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $solicitudes = $em->getRepository('AppBundle:Vinculo')->getSolicitudesRecibidas($usuario, Comunidad::ROL_ADMIN);

        return $this->render('vinculo/recibidas.html.twig', array(
                    'solicitudes' => $solicitudes,
        ));
    }

    /**
     * Registra la solicitud de vinculacion para el $idUsuario con la $idComunidad
     *
     * @Route("/new", name="vinculo_new")
     * @Method({"GET", "POST"})
     */
    public function agregarVinculoAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        # Agregar vinculo usuario/comunidad
        $idUsuario = $request->get('usuario');
        $usuario = $em->getRepository('AppBundle:Usuario')->find($idUsuario);

        $idComunidad = $request->get('comunidad');
        $comunidad = $em->getRepository('AppBundle:Comunidad')->find($idComunidad);

        $vinculo = new Vinculo();
        $vinculo->setComunidad($comunidad);
        $vinculo->setUsuario($usuario);
        $vinculo->setEstatus('P'); // Estatus = Pendiente
        $vinculo->setFechasolicitud(new \DateTime('now'));

        $em->persist($vinculo);
        $em->flush($vinculo);

        $bitacora = $this->get('BitacoraServices');
        $bitacora->agregarBitacora($usuario, "AGREGAR_VINCULO", "(" . $vinculo->getId() . ") U: " . $vinculo->getUsuario()->getId()." - C: ".$vinculo->getComunidad()->getId().", ".date_format($vinculo->getFechasolicitud(), 'd-m-Y H:i:s'));

        return $this->redirectToRoute('missolicitudes_index');
    }

    /**
     * Deletes a vinculo entity.
     *
     * @Route("/{id}", name="vinculo_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Vinculo $vinculo) {
        $form = $this->createDeleteForm($vinculo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($vinculo);
            $em->flush();
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * Creates a form to delete a vinculo entity.
     *
     * @param Vinculo $vinculo The vinculo entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Vinculo $vinculo) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('vinculo_delete', array('id' => $vinculo->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
