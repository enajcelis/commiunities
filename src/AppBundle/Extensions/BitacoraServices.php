<?php

namespace AppBundle\Extensions;

use AppBundle\Entity\Bitacora;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class BitacoraServices {
    
    protected $em;
    private $container;
    
    public function __construct(EntityManager $entityManager, Container $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
    }

    /**
     * Crea un registro de bitacora
     */
    public function agregarBitacora($username, $accion, $descripcion) {

        $bitacora = new Bitacora();
        $bitacora->setUsername($username);
        $bitacora->setAccion($accion);
        $bitacora->setDescripcion($descripcion);
        $bitacora->setFecha(new \DateTime('now'));

        $this->em->persist($bitacora);
        $this->em->flush();

        return true;
    }

}
