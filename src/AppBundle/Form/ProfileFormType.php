<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre', TextType::class, ['label' => 'profile.show.name', 'translation_domain' => 'FOSUserBundle', 'required' => true])
                ->add('apellido', TextType::class, ['label' => 'profile.show.lastname', 'translation_domain' => 'FOSUserBundle', 'required' => true])
                ->add('telefono', TextType::class, ['label' => 'profile.show.telephone', 'translation_domain' => 'FOSUserBundle', 'attr' => ['max_length' => 16 , 'min_length' => 8]])
                ->add('genero', ChoiceType::class, [
                                'choices' => [
                                    'M' => 'profile.show.female',
                                    'H' => 'profile.show.male',
                                ],
                                'placeholder' => 'profile.select.title',
                                'label' => 'profile.show.gender', 'translation_domain' => 'FOSUserBundle'
                ])
                ->add('fechaNacimiento', DateType::class, ['label' => 'profile.show.dateofbirth', 'translation_domain' => 'FOSUserBundle', 'widget' => 'single_text'])
                ->add('imageFile', FileType::class, array('label' => 'profile.show.picture', 'translation_domain' => 'FOSUserBundle', 'data_class' => null, 'required' => false))
                ->add('direccion', new DireccionUsuarioType(), array('label' => 'direccion.titulo', 'translation_domain' => 'commiunities'));
    }

public function getParent() 
{
    return 'FOS\UserBundle\Form\Type\ProfileFormType';
}

public function getBlockPrefix()
{
    return 'app_user_profile';
}

// For Symfony 2.x
public function getName()
{
    return $this->getBlockPrefix();
}

}