<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EquipamientoType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $this->comunidades = $options['comunidades'];
        $this->usuario = $options['usuario'];

        $builder->add('comunidad', 'entity', array('label' => 'equipamiento.tabla.comunidad',
                    'translation_domain' => 'commiunities',
                    'class' => 'AppBundle:Comunidad',
                    'choices' => $this->comunidades,
                    'property' => 'nombre',
                    'empty_value' => 'select.titulo',
                    'required' => true,
                    'attr' => array('class' => 'form-control')
                ))
                ->add('equipo', 'entity', array('label' => 'equipamiento.tabla.equipo',
                    'translation_domain' => 'commiunities',
                    'class' => 'AppBundle:Equipo',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('e')
                                ->where('e.usuario = ?1')
                                ->orderBy('e.nombre', 'ASC')
                                ->setParameter(1, $this->usuario);
                    },
                    'property' => 'nombre',
                    'empty_value' => 'select.titulo',
                    'required' => true,
                    'attr' => array('class' => 'form-control')
                ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Equipamiento',
            'usuario' => 0,
            'comunidades' => null,
        ));

        $resolver->setRequired('usuario'); // Requires that currentOrg be set by the caller.
        $resolver->setAllowedTypes('usuario', 'AppBundle\Entity\Usuario'); // Validates the type(s) of option(s) passed.
        $resolver->setRequired('comunidades'); 
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'appbundle_equipamiento';
    }

}
