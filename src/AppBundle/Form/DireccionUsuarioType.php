<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType; 
use Symfony\Component\Form\Extension\Core\Type\TextType; 
use Symfony\Component\Form\Extension\Core\Type\CountryType;

class DireccionUsuarioType extends DireccionType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('calle', NumberType::class, array(
                            'label' => 'direccion.calle', 
                            'translation_domain' => 'commiunities'))
                ->add('numero', NumberType::class, array(
                            'label' => 'direccion.numero', 
                            'translation_domain' => 'commiunities'))
                ->add('piso', NumberType::class, array(
                            'label' => 'direccion.piso', 
                            'translation_domain' => 'commiunities', 
                            'required' => false))
                ->add('puerta', NumberType::class, array(
                            'label' => 'direccion.puerta', 
                            'translation_domain' => 'commiunities', 
                            'required' => false))
                ->add('codigopostal', TextType::class, array(
                            'label' => 'direccion.codigopostal', 
                            'translation_domain' => 'commiunities'))
                ->add('pais', CountryType::class, array(
                            'label' => 'direccion.pais', 
                            'translation_domain' => 'commiunities', 
                            'placeholder' => 'select.titulo'))
                ->add('ciudad', TextType::class, array(
                            'label' => 'direccion.ciudad', 
                            'translation_domain' => 'commiunities'))           
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Direccion'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_direccion';
    }


}
