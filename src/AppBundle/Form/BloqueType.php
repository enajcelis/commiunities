<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class BloqueType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $this->ideq = $options['ideq'];

        $builder->add('equipamiento', 'entity', array('label' => 'equipamiento.titulo',
                    'translation_domain' => 'commiunities',
                    'class' => 'AppBundle:Equipamiento',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('e')
                                ->where('e.id = ?1')
                                ->setParameter(1, $this->ideq);
                    },
                    'property' => 'id',
                    'attr' => array('class' => 'form-control')
                ))
                ->add('dia', ChoiceType::class, [
                    'choices' => [
                        'LU' => 'bloque.dia.lunes',
                        'MA' => 'bloque.dia.martes',
                        'MI' => 'bloque.dia.miercoles',
                        'JU' => 'bloque.dia.jueves',
                        'VI' => 'bloque.dia.viernes',
                        'SA' => 'bloque.dia.sabado',
                        'DO' => 'bloque.dia.domingo'
                    ],
                    'placeholder' => 'select.titulo', 'translation_domain' => 'commiunities',
                    'label' => 'bloque.dia.titulo'
                ])
                ->add('horainicio', TimeType::class, array(
                    'label' => 'bloque.hora.inicio',
                    'translation_domain' => 'commiunities',
                    'minutes' => array(0, 10, 20, 30, 40, 50),
                    'placeholder' => array(
                        'hour' => 'bloque.hora.hora', 'minute' => 'bloque.hora.minuto')
                ))
                ->add('horafin', TimeType::class, array(
                    'label' => 'bloque.hora.fin',
                    'translation_domain' => 'commiunities',
                    'minutes' => array(0, 10, 20, 30, 40, 50),
                    'placeholder' => array(
                        'hour' => 'bloque.hora.hora', 'minute' => 'bloque.hora.minuto')
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Bloque',
            'ideq' => 0,
        ));

        $resolver->setRequired('ideq'); // Requires that currentOrg be set by the caller.
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'appbundle_bloque';
    }

}
