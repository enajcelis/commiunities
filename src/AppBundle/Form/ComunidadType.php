<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType; 
use Symfony\Component\Form\Extension\Core\Type\TextType; 

class ComunidadType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre', TextType::class, array('label' => 'comunidades.nombre', 'translation_domain' => 'commiunities')) 
                ->add('descripcion', TextType::class, array('label' => 'comunidades.descripcion', 'translation_domain' => 'commiunities')) 
                ->add('imageFile', FileType::class, array('label' => 'comunidades.foto', 'data_class' => null, 'required' => false, 'translation_domain' => 'commiunities')) 
                ->add('normativa', 'ckeditor', array('label' => 'comunidades.normativa', 'translation_domain' => 'commiunities')) 
                ->add('direccion', new DireccionType(), array('label' => 'direccion.titulo', 'translation_domain' => 'commiunities'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Comunidad'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_comunidad';
    }


}
