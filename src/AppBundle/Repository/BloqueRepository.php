<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class BloqueRepository extends EntityRepository 
{  
    /**
     * Retorna un arreglo con los ids de los equipamientos de las comunidades vinculadas 
     * al $usuario segun el $rol especificado
     */
    public function getIdBloques($usuario, $rol) {
        
        $idBloques[] = array();

        $bloques = $this->getEntityManager()
                ->createQuery(
                        'SELECT b '
                        . 'FROM AppBundle:Privilegio p, AppBundle:Equipamiento e, AppBundle:Bloque b '
                        . 'WHERE b.equipamiento = e.id '
                        . 'AND e.comunidad = p.comunidad '
                        . 'AND p.usuario = ?1 '
                        . 'AND p.rol = ?2 '
                )
                ->setParameter(1, $usuario)
                ->setParameter(2, $rol)
                ->getResult();

        foreach ($bloques as $bq) {
            $idBloques[] = $bq->getId();
        }

        return $idBloques;
    }

}
