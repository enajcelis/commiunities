<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class VinculoRepository extends EntityRepository {

    /**
     * Retorna un arreglo con los ids de los vinculos asociados al $usuario
     */
    public function getIdVinculos($comunidades, $usuario) {

        $idVinculos = null;

        foreach ($comunidades as $comunidad) {
            $vinculos = $this->getEntityManager()
                    ->createQuery(
                            'SELECT v '
                            . 'FROM AppBundle:Vinculo v '
                            . 'WHERE v.usuario != ?1 '
                            . 'AND v.comunidad = ?2 '
                    )
                    ->setParameter(1, $usuario)
                    ->setParameter(2, $comunidad)
                    ->getResult();

            foreach ($vinculos as $v) {
                $idVinculos[] = $v->getId();
            }            
        }
        return $idVinculos;
    }

    /**
     * Modifica el $vinculo especificado
     */
    public function updateVinculo($vinculo, $accion) {

        if ($accion == 'a') {
            return $this->getEntityManager()
                            ->createQuery(
                                    'UPDATE AppBundle:Vinculo v '
                                    . 'SET v.estatus = ?1, v.fechaaprobacion = ?2'
                                    . 'WHERE v.id = ?3'
                            )
                            ->setParameter(1, 'A')
                            ->setParameter(2, new \DateTime('now'))
                            ->setParameter(3, $vinculo)
                            ->getResult();
        } elseif ($accion == 'n') {
            return $this->getEntityManager()
                            ->createQuery(
                                    'UPDATE AppBundle:Vinculo v '
                                    . 'SET v.estatus = ?1 '
                                    . 'WHERE v.id = ?2'
                            )
                            ->setParameter(1, 'N')
                            ->setParameter(2, $vinculo)
                            ->getResult();
        }
    }

    /**
     * Buscar si hay solicitudes de vinculo 'P' (pendiente) o 'N' (negada) para la $comunidad y $usuario especificado,
     * para estos casos no se muestra el boton de Vinculacion en el detalle de la $comunidad
     */
    public function getVinculoComunidad($comunidad, $usuario) {

        return $this->getEntityManager()
                        ->createQuery(
                                'SELECT v '
                                . 'FROM AppBundle:Vinculo v '
                                . 'WHERE v.comunidad = ?1 '
                                . 'AND v.usuario = ?2 '
                                . 'AND (v.estatus = \'P\' OR v.estatus = \'N\') '
                        )
                        ->setParameter(1, $comunidad)
                        ->setParameter(2, $usuario)
                        ->getResult();
    }

    /**
     * Retorna las solicitudes de vinculacion realizadas por el $usuario en sesion
     */
    public function getMisSolicitudes($usuario) {

        return $this->getEntityManager()
                        ->createQuery(
                                'SELECT v '
                                . 'FROM AppBundle:Vinculo v '
                                . 'WHERE v.usuario = ?1 '
                                . 'ORDER BY v.fechasolicitud'
                        )
                        ->setParameter(1, $usuario)
                        ->getResult();
    }

    /**
     * Retorna las solicitudes de vinculacion a las comunidades del $usuario en sesion
     */
    public function getSolicitudesRecibidas($usuario, $rol) {

        return $this->getEntityManager()
                        ->createQuery(
                                'SELECT v '
                                . 'FROM AppBundle:Vinculo v, AppBundle:Privilegio p '
                                . 'WHERE v.comunidad = p.comunidad '
                                . 'AND v.usuario != ?1 '
                                . 'AND p.rol = ?2 '
                                . 'AND p.usuario = ?1'
                                . 'ORDER BY v.fechasolicitud'
                        )
                        ->setParameter(1, $usuario)
                        ->setParameter(2, $rol)
                        ->getResult();
    }

}
