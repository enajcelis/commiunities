<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PrivilegioRepository extends EntityRepository {

    /**
     * Modifica el $privilegio para colocar al $usuario el $rol especificado
     */
    public function updatePrivilegio($comunidad, $usuario, $rol) {

        return $this->getEntityManager()
                        ->createQuery(
                                'UPDATE AppBundle:Privilegio p '
                                . 'SET p.rol = ?1 '
                                . 'WHERE p.comunidad = ?2 '
                                . 'AND p.usuario = ?3'
                        )
                        ->setParameter(1, $rol)
                        ->setParameter(2, $comunidad)
                        ->setParameter(3, $usuario)
                        ->getResult();
    }

    /**
     * Retorna la lista de usuarios miembros (rol = 2) de la $comunidad especificada
     */
    public function getMiembrosComunidad($comunidad, $rol) {

        return $this->getEntityManager()
                        ->createQuery(
                                'SELECT u '
                                . 'FROM AppBundle:Usuario u, AppBundle:Privilegio p '
                                . 'WHERE p.usuario = u.id '
                                . 'AND p.comunidad = ?1 '
                                . 'AND p.rol = ?2 '
                        )
                        ->setParameter(1, $comunidad)
                        ->setParameter(2, $rol)
                        ->getResult();
    }

    /**
     * Lista la cantidad de usuarios (miembros) vinculados a la comunidad especificada
     */
    public function getCantidadMiembros($comunidad) {
        $stmt = $this->getEntityManager()
                ->getConnection()
                ->prepare('SELECT COUNT(usuario_id) AS cantidad FROM privilegio WHERE comunidad_id = :id');
        $stmt->bindValue('id', $comunidad);
        $stmt->execute();

        return $stmt->fetch();
    }

    /**
     * Retorna la cantidad de comunidades asociadas al $usuario segun el $rol especificado
     */
    public function cantidadComunidades($usuario, $rol) {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('count(p)')
                ->from('AppBundle:Privilegio', 'p')
                ->where('p.usuario = :usuario and p.rol = :rol')
                ->setParameter('usuario', $usuario)
                ->setParameter('rol', $rol);

        $query = $qb->getQuery();
        $cantidad = $query->getSingleScalarResult();

        return $cantidad;
    }

    /**
     * Retorna el rol para la $comunidad y $usuario especificados
     */
    public function getPrivilegioRol($usuario, $comunidad) {

        $rol = $this->getEntityManager()
                ->createQuery(
                        'SELECT r '
                        . 'FROM AppBundle:Privilegio p, AppBundle:Comunidad c, AppBundle:Rol r '
                        . 'WHERE c.id = p.comunidad AND p.rol = r.id AND '
                        . 'p.comunidad = ?1 AND p.usuario = ?2 '
                )
                ->setParameter(1, $comunidad)
                ->setParameter(2, $usuario)
                ->getResult();

        if ($rol) {
            return $rol[0]->getId();
        } else {
            return "";
        }
    }

}
