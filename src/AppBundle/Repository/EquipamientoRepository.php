<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Comunidad;

class EquipamientoRepository extends EntityRepository 
{  
    /**
     * Retorna un arreglo con los ids de las equipamientos del $usuario
     */
    public function getIdEquipamientosUsuario($usuario) {
        
        $idEquipamientos = null;
        
        $equipamientos = $this->getEntityManager()
                ->createQuery(
                        'SELECT e '
                        . 'FROM AppBundle:Privilegio p, AppBundle:Equipamiento e '
                        . 'WHERE p.comunidad = e.comunidad '
                        . 'AND p.usuario = ?1 '
                )
                ->setParameter(1, $usuario)
                ->getResult();

        foreach ($equipamientos as $eq) {
            $idEquipamientos[] = $eq->getId();
        }

        return $idEquipamientos;
    }
    
    /**
     * Retorna un arreglo con los ids de los equipamientos de las comunidades vinculadas 
     * al $usuario segun el $rol especificado
     */
    public function getIdEquipamientos($usuario, $rol) {
        
        $idEquipamientos[] = array();

        $equipamientos = $this->getEntityManager()
                ->createQuery(
                        'SELECT e '
                        . 'FROM AppBundle:Privilegio p, AppBundle:Equipamiento e '
                        . 'WHERE p.comunidad = e.comunidad '
                        . 'AND p.usuario = ?1 '
                        . 'AND p.rol = ?2 '
                )
                ->setParameter(1, $usuario)
                ->setParameter(2, $rol)
                ->getResult();

        foreach ($equipamientos as $eq) {
            $idEquipamientos[] = $eq->getId();
        }

        return $idEquipamientos;
    }
    
    /**
     * Retorna los equipamientos del $usuario especificado
     */
    public function getEquipamientos($usuario) {
        
        return $this->getEntityManager()
                        ->createQuery(
                                'SELECT e '
                                . 'FROM AppBundle:Privilegio p, AppBundle:Equipamiento e '
                                . 'WHERE e.comunidad = p.comunidad AND p.usuario = ?1 AND p.rol = ?2'
                                . 'ORDER BY e.comunidad'
                        )
                        ->setParameter(1, $usuario)
                        ->setParameter(2, Comunidad::ROL_ADMIN)
                        ->getResult();
    }
}
