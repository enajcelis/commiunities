<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ReservaRepository extends EntityRepository 
{        
    /**
     * Retorna un arreglo con los ids de las reservas del $usuario
     */
    public function getIdReservas($usuario) {
        
        $idReservas = null;
        
        $reservas = $this->getEntityManager()
                ->createQuery(
                        'SELECT r '
                        . 'FROM AppBundle:Reserva r '
                        . 'WHERE r.usuario = ?1 '
                )
                ->setParameter(1, $usuario)
                ->getResult();

        foreach ($reservas as $res) {
            $idReservas[] = $res->getId();
        }

        return $idReservas;
    }
    
}
