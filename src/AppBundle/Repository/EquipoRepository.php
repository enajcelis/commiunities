<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class EquipoRepository extends EntityRepository {

    /**
     * Retorna un arreglo con los ids de los equipos del $usuario especificado, 
     * que tienen equipamientos asociados para la $comunidad
     */
    public function getidEquiposCeder($usuario, $comunidad) {

        $idEquipos = null;

        $equipos = $this->getEntityManager()
                ->createQuery(
                        'SELECT e '
                        . 'FROM AppBundle:Equipo e, AppBundle:Equipamiento eq '
                        . 'WHERE e.id = eq.equipo '
                        . 'AND e.usuario = ?1 '
                        . 'AND eq.comunidad = ?2 '
                )
                ->setParameter(1, $usuario)
                ->setParameter(2, $comunidad)
                ->getResult();

        foreach ($equipos as $eq) {
            $idEquipos[] = $eq->getId();
        }

        return $idEquipos;
    }

    /**
     * Actualiza el usuario Adminstrador de los equipos
     * Se realiza una vez cedida la adminstracion de una cominidad
     */
    public function updateEquipoAdmin($idEquipo, $usuarioAdmin) {

        return $this->getEntityManager()
                        ->createQuery(
                                'UPDATE AppBundle:Equipo e '
                                . 'SET e.usuario = ?1 '
                                . 'WHERE e.id = ?2'
                        )
                        ->setParameter(1, $usuarioAdmin)
                        ->setParameter(2, $idEquipo)
                        ->getResult();
    }

}
