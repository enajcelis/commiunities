<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ComunidadRepository extends EntityRepository 
{        
    /**
     * Retorna un arreglo con los ids de las comunidades del $usuario
     */
    public function getIdComunidadesUsuario($usuario) {
        
        $idComunidades = null;
        
        $comunidades = $this->getEntityManager()
                ->createQuery(
                        'SELECT c '
                        . 'FROM AppBundle:Privilegio p, AppBundle:Comunidad c '
                        . 'WHERE p.comunidad = c.id  '
                        . 'AND p.usuario = ?1 '
                        . 'ORDER BY c.nombre'
                )
                ->setParameter(1, $usuario)
                ->getResult();

        foreach ($comunidades as $com) {
            $idComunidades[] = $com->getId();
        }

        return $idComunidades;
    }
    
    /**
     * Retorna las $comunidades del $usuario segun el $rol especificado
     */
    public function getComunidades($usuario, $rol) {
        
        $misComunidades = null;
        
        $comunidades = $this->getEntityManager()
                ->createQuery(
                        'SELECT c '
                        . 'FROM AppBundle:Privilegio p, AppBundle:Comunidad c '
                        . 'WHERE p.comunidad = c.id  '
                        . 'AND p.usuario = ?1 '
                        . 'AND p.rol = ?2 '
                        . 'ORDER BY c.nombre'
                )
                ->setParameter(1, $usuario)
                ->setParameter(2, $rol)
                ->getResult();

        foreach ($comunidades as $com) {
            $misComunidades[] = $com;
        }

        return $misComunidades;
    }
    
    /**
     * Retorna un arreglo con los ids de las comunidades del $usuario segun el $rol especificado
     */
    public function getIdComunidades($usuario, $rol) {
        
        $idComunidades[] = array();
        
        $comunidades = $this->getEntityManager()
                ->createQuery(
                        'SELECT c '
                        . 'FROM AppBundle:Privilegio p, AppBundle:Comunidad c '
                        . 'WHERE p.comunidad = c.id  '
                        . 'AND p.usuario = ?1 '
                        . 'AND p.rol = ?2 '
                        . 'ORDER BY c.nombre'
                )
                ->setParameter(1, $usuario)
                ->setParameter(2, $rol)
                ->getResult();

        foreach ($comunidades as $com) {
            $idComunidades[] = $com->getId();
        }

        return $idComunidades;
    }
    
    /**
     * Retorna las comunidades del $usuario y con el $rol especificado
     * Si $rol es 0 se filtran todos los roles
     */
    public function getComunidadesPrivilegio($usuario, $rol) {
        if ($rol == 0) {
            return $this->getEntityManager()
                            ->createQuery(
                                    'SELECT c '
                                    . 'FROM AppBundle:Privilegio p, AppBundle:Comunidad c '
                                    . 'WHERE c.id = p.comunidad '
                                    . 'AND p.usuario = ?1 '
                                    . 'ORDER BY c.nombre'
                            )
                            ->setParameter(1, $usuario)
                            ->getResult();
        } else {
            return $this->getEntityManager()
                            ->createQuery(
                                    'SELECT c '
                                    . 'FROM AppBundle:Privilegio p, AppBundle:Comunidad c '
                                    . 'WHERE c.id = p.comunidad '
                                    . 'AND p.usuario = ?1 '
                                    . 'AND p.rol = ?2 '
                                    . 'ORDER BY c.nombre'
                            )
                            ->setParameter(1, $usuario)
                            ->setParameter(2, $rol)
                            ->getResult();
        }
    }

}
