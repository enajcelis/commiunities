<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Privilegio
 *
 * @ORM\Table(name="privilegio", indexes={@ORM\Index(name="fk_usuario_has_rol_rol1_idx", columns={"rol_id"}), @ORM\Index(name="fk_usuario_has_rol_usuario1_idx", columns={"usuario_id"}), @ORM\Index(name="fk_usuario_has_rol_comunidad1_idx", columns={"comunidad_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PrivilegioRepository")
 */
class Privilegio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Comunidad
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Comunidad")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="comunidad_id", referencedColumnName="id")
     * })
     */
    private $comunidad;

    /**
     * @var \AppBundle\Entity\Rol
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Rol")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rol_id", referencedColumnName="id")
     * })
     */
    private $rol;

    /**
     * @var \AppBundle\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comunidad
     *
     * @param \AppBundle\Entity\Comunidad $comunidad
     * @return Privilegio
     */
    public function setComunidad(\AppBundle\Entity\Comunidad $comunidad = null)
    {
        $this->comunidad = $comunidad;

        return $this;
    }

    /**
     * Get comunidad
     *
     * @return \AppBundle\Entity\Comunidad 
     */
    public function getComunidad()
    {
        return $this->comunidad;
    }

    /**
     * Set rol
     *
     * @param \AppBundle\Entity\Rol $rol
     * @return Privilegio
     */
    public function setRol(\AppBundle\Entity\Rol $rol = null)
    {
        $this->rol = $rol;

        return $this;
    }

    /**
     * Get rol
     *
     * @return \AppBundle\Entity\Rol 
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     * @return Privilegio
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}
