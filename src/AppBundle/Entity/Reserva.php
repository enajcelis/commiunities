<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Reserva
 *
 * @ORM\Table(name="reserva", indexes={@ORM\Index(name="fk_reserva_equipamiento1_idx", columns={"equipamiento_id"}), @ORM\Index(name="fk_reserva_bloque1_idx", columns={"bloque_id"}), @ORM\Index(name="fk_reserva_usuario1_idx", columns={"usuario_id"}), @ORM\Index(name="fk_reserva_comunidad1_idx", columns={"comunidad_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReservaRepository")
 */
class Reserva
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="horaDesde", type="time", nullable=false)
     */
    private $horadesde;

    /**
     * @var \DateTime
     * @Assert\Time()   
     * @Assert\Expression(
     *      "this.getHoradesde() < this.getHorahasta()",
     *      message="bloque.hora.posterior"
     * )     *
     * @ORM\Column(name="horaHasta", type="time", nullable=false)
     */
    private $horahasta;

    /**
     * @var \Bloque
     *
     * @ORM\ManyToOne(targetEntity="Bloque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bloque_id", referencedColumnName="id")
     * })
     */
    private $bloque;

    /**
     * @var \Comunidad
     *
     * @ORM\ManyToOne(targetEntity="Comunidad")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="comunidad_id", referencedColumnName="id")
     * })
     */
    private $comunidad;

    /**
     * @var \Equipamiento
     *
     * @ORM\ManyToOne(targetEntity="Equipamiento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="equipamiento_id", referencedColumnName="id")
     * })
     */
    private $equipamiento;

    /**
     * @var \Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Reserva
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set horadesde
     *
     * @param \DateTime $horadesde
     * @return Reserva
     */
    public function setHoradesde($horadesde)
    {
        $this->horadesde = $horadesde;

        return $this;
    }

    /**
     * Get horadesde
     *
     * @return \DateTime 
     */
    public function getHoradesde()
    {
        return $this->horadesde;
    }

    /**
     * Set horahasta
     *
     * @param \DateTime $horahasta
     * @return Reserva
     */
    public function setHorahasta($horahasta)
    {
        $this->horahasta = $horahasta;

        return $this;
    }

    /**
     * Get horahasta
     *
     * @return \DateTime 
     */
    public function getHorahasta()
    {
        return $this->horahasta;
    }

    /**
     * Set bloque
     *
     * @param \AppBundle\Entity\Bloque $bloque
     * @return Reserva
     */
    public function setBloque(\AppBundle\Entity\Bloque $bloque = null)
    {
        $this->bloque = $bloque;

        return $this;
    }

    /**
     * Get bloque
     *
     * @return \AppBundle\Entity\Bloque 
     */
    public function getBloque()
    {
        return $this->bloque;
    }

    /**
     * Set comunidad
     *
     * @param \AppBundle\Entity\Comunidad $comunidad
     * @return Reserva
     */
    public function setComunidad(\AppBundle\Entity\Comunidad $comunidad = null)
    {
        $this->comunidad = $comunidad;

        return $this;
    }

    /**
     * Get comunidad
     *
     * @return \AppBundle\Entity\Comunidad 
     */
    public function getComunidad()
    {
        return $this->comunidad;
    }

    /**
     * Set equipamiento
     *
     * @param \AppBundle\Entity\Equipamiento $equipamiento
     * @return Reserva
     */
    public function setEquipamiento(\AppBundle\Entity\Equipamiento $equipamiento = null)
    {
        $this->equipamiento = $equipamiento;

        return $this;
    }

    /**
     * Get equipamiento
     *
     * @return \AppBundle\Entity\Equipamiento 
     */
    public function getEquipamiento()
    {
        return $this->equipamiento;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     * @return Reserva
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}
