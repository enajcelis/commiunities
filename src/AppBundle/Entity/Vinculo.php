<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vinculo
 *
 * @ORM\Table(name="vinculo", indexes={@ORM\Index(name="fk_usuario_has_comunidad_comunidad1_idx", columns={"comunidad_id"}), @ORM\Index(name="fk_usuario_has_comunidad_usuario1_idx", columns={"usuario_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VinculoRepository")
 */
class Vinculo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="estatus", type="string", length=10, nullable=false)
     */
    private $estatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaSolicitud", type="datetime", nullable=false)
     */
    private $fechasolicitud;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaAprobacion", type="datetime", nullable=false)
     */
    private $fechaaprobacion;

    /**
     * @var \AppBundle\Entity\Comunidad
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Comunidad")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="comunidad_id", referencedColumnName="id")
     * })
     */
    private $comunidad;

    /**
     * @var \AppBundle\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estatus
     *
     * @param string $estatus
     * @return Vinculo
     */
    public function setEstatus($estatus)
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * Get estatus
     *
     * @return string 
     */
    public function getEstatus()
    {
        return $this->estatus;
    }

    /**
     * Set fechasolicitud
     *
     * @param \DateTime $fechasolicitud
     * @return Vinculo
     */
    public function setFechasolicitud($fechasolicitud)
    {
        $this->fechasolicitud = $fechasolicitud;

        return $this;
    }

    /**
     * Get fechasolicitud
     *
     * @return \DateTime 
     */
    public function getFechasolicitud()
    {
        return $this->fechasolicitud;
    }

    /**
     * Set fechaaprobacion
     *
     * @param \DateTime $fechaaprobacion
     * @return Vinculo
     */
    public function setFechaaprobacion($fechaaprobacion)
    {
        $this->fechaaprobacion = $fechaaprobacion;

        return $this;
    }

    /**
     * Get fechaaprobacion
     *
     * @return \DateTime 
     */
    public function getFechaaprobacion()
    {
        return $this->fechaaprobacion;
    }

    /**
     * Set comunidad
     *
     * @param \AppBundle\Entity\Comunidad $comunidad
     * @return Vinculo
     */
    public function setComunidad(\AppBundle\Entity\Comunidad $comunidad = null)
    {
        $this->comunidad = $comunidad;

        return $this;
    }

    /**
     * Get comunidad
     *
     * @return \AppBundle\Entity\Comunidad 
     */
    public function getComunidad()
    {
        return $this->comunidad;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     * @return Vinculo
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}
