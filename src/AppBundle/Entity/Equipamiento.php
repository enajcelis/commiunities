<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Equipamiento
 *
 * @ORM\Table(name="equipamiento", indexes={@ORM\Index(name="fk_equipo_has_comunidad_comunidad1_idx", columns={"comunidad_id"}), @ORM\Index(name="fk_equipo_has_comunidad_equipo_idx", columns={"equipo_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EquipamientoRepository")
 */
class Equipamiento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Comunidad
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Comunidad")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="comunidad_id", referencedColumnName="id")
     * })
     */
    private $comunidad;

    /**
     * @var \AppBundle\Entity\Equipo
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="equipo_id", referencedColumnName="id")
     * })
     */
    private $equipo;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comunidad
     *
     * @param \AppBundle\Entity\Comunidad $comunidad
     * @return Equipamiento
     */
    public function setComunidad(\AppBundle\Entity\Comunidad $comunidad = null)
    {
        $this->comunidad = $comunidad;

        return $this;
    }

    /**
     * Get comunidad
     *
     * @return \AppBundle\Entity\Comunidad 
     */
    public function getComunidad()
    {
        return $this->comunidad;
    }

    /**
     * Set equipo
     *
     * @param \AppBundle\Entity\Equipo $equipo
     * @return Equipamiento
     */
    public function setEquipo(\AppBundle\Entity\Equipo $equipo = null)
    {
        $this->equipo = $equipo;

        return $this;
    }

    /**
     * Get equipo
     *
     * @return \AppBundle\Entity\Equipo 
     */
    public function getEquipo()
    {
        return $this->equipo;
    }
}
