<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Bloque
 *
 * @ORM\Table(name="bloque", indexes={@ORM\Index(name="fk_bloque_equipamiento1_idx", columns={"equipamiento_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BloqueRepository")
 */
class Bloque
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="dia", type="string", length=2, nullable=false)
     */
    private $dia;

    /**
     * @var \DateTime
     * @Assert\Time()
     * @ORM\Column(name="horaInicio", type="time", nullable=false)
     */
    private $horainicio;

    /**
     * @var \DateTime
     * @Assert\Time()   
     * @Assert\Expression(
     *      "this.getHorainicio() < this.getHorafin()",
     *      message="bloque.hora.posterior"
     * )
     * @ORM\Column(name="horaFin", type="time", nullable=false)
     */
    private $horafin;

    /**
     * @var \Equipamiento
     *
     * @ORM\ManyToOne(targetEntity="Equipamiento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="equipamiento_id", referencedColumnName="id")
     * })
     */
    private $equipamiento;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dia
     *
     * @param string $dia
     * @return Bloque
     */
    public function setDia($dia)
    {
        $this->dia = $dia;

        return $this;
    }

    /**
     * Get dia
     *
     * @return string 
     */
    public function getDia()
    {
        return $this->dia;
    }

    /**
     * Set horainicio
     *
     * @param \DateTime $horainicio
     * @return Bloque
     */
    public function setHorainicio($horainicio)
    {
        $this->horainicio = $horainicio;

        return $this;
    }

    /**
     * Get horainicio
     *
     * @return \DateTime 
     */
    public function getHorainicio()
    {
        return $this->horainicio;
    }

    /**
     * Set horafin
     *
     * @param \DateTime $horafin
     * @return Bloque
     */
    public function setHorafin($horafin)
    {
        $this->horafin = $horafin;

        return $this;
    }

    /**
     * Get horafin
     *
     * @return \DateTime 
     */
    public function getHorafin()
    {
        return $this->horafin;
    }

    /**
     * Set equipamiento
     *
     * @param \AppBundle\Entity\Equipamiento $equipamiento
     * @return Bloque
     */
    public function setEquipamiento(\AppBundle\Entity\Equipamiento $equipamiento = null)
    {
        $this->equipamiento = $equipamiento;

        return $this;
    }

    /**
     * Get equipamiento
     *
     * @return \AppBundle\Entity\Equipamiento 
     */
    public function getEquipamiento()
    {
        return $this->equipamiento;
    }
}
