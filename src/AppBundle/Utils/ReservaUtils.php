<?php

namespace AppBundle\Utils;

class ReservaUtils {

    public function __construct($start, $end) {
        $this->start = $start;
        $this->end = $end;
    }

    public function starttime() {
        list($hour, $minute) = explode(":", $this->start);
        return (int) $hour * 60 + (int) $minute;
    }

    public function endtime() {
        list($hour, $minute) = explode(":", $this->end);
        return (int) $hour * 60 + (int) $minute;
    }

}
